package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.Collections;

import static java.util.Comparator.comparingInt;

public class Main {
    static String path;
    static List<List<String>> allGroups= new ArrayList<>();
    static List<String> startingGroup= new ArrayList<>();

    public static void main(String[] args){
        path=args[0];
        long start = System.currentTimeMillis();
        try {
            startingGroup=readTxtToList();
        }
        catch (IOException fileNotFoundException) {
            System.out.println("\nError: File cannot be found. \nThere is a possibility that the path to the file is wrong. Try checking the name, using an absolute path or moving the file.");
            return;
        }
        allGroups=separateGroup(startingGroup);
        allGroups.removeIf(list -> (list.size() < 2));
        allGroups.sort(comparingInt(List::size));
        System.out.println("\nTotal Groups (excluding groups with 1 element): "+ (allGroups.size())+"\n");
        Integer groupNum=1;
        //System.out.println(Arrays.toString(allGroups.toArray()));
        for (int i = allGroups.size()-1;i>=0;i--) {
            System.out.println("Group Number "+groupNum + "\n");
            for (int j = 0;j<allGroups.get(i).size();j++) {
                System.out.println(allGroups.get(i).get(j) + "\n");
            }
            groupNum+=1;
        }
        float sec = (System.currentTimeMillis() - start) / 1000F; System.out.println("Program was finished in "+sec + " seconds");
    }

    private static List<List<String>> separateGroup(List<String> lines) {
        if (lines == null)
            return Collections.emptyList();
        List<List<String>> finalGroups = new ArrayList<>(); //Список групп, который мы будем возвращать. Каждая группа должна быть представлена как список входящих в группу строк.
        List<Map<String, Integer>> columnList = new ArrayList<>(); // Список колонок. Поскольку столбцов меньше колонок - быстрее смотреть по ним. Представлен как Мап, где ключ - элемент, а значение - группа к которой он определен.
        Map<Integer, Integer> finalUnited = new HashMap<>(); // Список в виде Мапа, нужен для последующего объединения групп. Ключ - номер группы, значение - номер группы, которая должна быть объединена с группой в ключе"
        for (int i = 0; i < lines.size(); i++) { //Входим в цикл для каждой линии
            String line = lines.get(i); //Берем новую линию для работы
            if (!isBadLine(line, lines)) { //Проверяем линию на валидность. Если невалидна - убираем ее из списка и пропускаем распределение в группу
                String[] lineArray = line.split(";"); //Разделяем линии на элементы чтобы проще отследить колонки
                TreeSet<Integer> sameElementStoreTree = new TreeSet<>(); //Дерево поиска (не список из-за эффективности по времени), отражающее набор групп, у которых есть одинаковые элементы
                List<Map<String,Integer>> uniqueElements= new ArrayList<>(); //Список уникальных (пока что) значений которых нет в столбцах других строк
                for (int currentColumn = 0; currentColumn < lineArray.length; currentColumn++) { //Пока в строке есть элементы
                    if (columnList.size() == currentColumn) //Если колонок недостаточно - добавляем новую.
                        columnList.add(new HashMap<>());
                    String currentValue= lineArray[currentColumn]; // Читаем элементы
                    if ("\"\"".equals(currentValue)) //Если элемента нет - пропускаем его и сразу приступаем к следующему.
                        continue;
                    Map<String, Integer> wholeColumn = columnList.get(currentColumn);  //Создаем мап для проверки текущей колонки
                    Integer elementPlace = wholeColumn.get(currentValue); //И присваиваем новому int значение место того же значения, что мы ищем
                    if (elementPlace != null) { //Если такие элементы есть
                        while (finalUnited.containsKey(elementPlace)) // И если группа с этим номером раньше была объединена с другой
                            elementPlace = finalUnited.get(elementPlace); //То указываем что все группы должны быть объединены
                        sameElementStoreTree.add(elementPlace); //Добавляем номер найденного элемента в дерево
                    } else {
                        Map<String,Integer> currentMap = new HashMap<>(); //Если не нашли - новый хешмап кладем в новые элементы.
                        currentMap.put(currentValue,currentColumn);
                        uniqueElements.add(currentMap);
                    }
                }
                int groupNumber; //Когда элементы строки заканчиваются
                if (sameElementStoreTree.isEmpty()) { //И если дерево групп для объединения пустое
                    finalGroups.add(new ArrayList<>()); //То делаем новую группу.
                    groupNumber = finalGroups.size() - 1; //Ее номер - максимальный из всех что есть
                } else { //Если дерево не пустое
                    groupNumber = sameElementStoreTree.first(); //То ее группа та же что и у первого элемента
                }
                for (Map<String,Integer> newLine : uniqueElements) { //Кладем в список колонок новый элемент
                    for (Map.Entry<String, Integer> entry : newLine.entrySet()) {
                        columnList.get(entry.getValue()).put(entry.getKey(), groupNumber);
                    }
                }
                //И начинаем процесс объединения групп
                for (int mathedGroup : sameElementStoreTree) { //Каждая группа с совпадающим элементом проходит в цикл
                    if (mathedGroup != groupNumber) { //При условии что эта группа уже не была объединена
                        finalUnited.put(mathedGroup, groupNumber); // Помечаем группу как объединенную
                        finalGroups.get(groupNumber).addAll(finalGroups.get(mathedGroup)); //И объединяем группы
                        finalGroups.set(mathedGroup, null); //А номер группы удаляем для того чтобы не возникало ошибок далее
                    }
                }
                if (!(finalGroups.get(groupNumber).contains(line))) {
                    finalGroups.get(groupNumber).add(line);
                }
            }
        }
        finalGroups.removeAll(Collections.singleton(null)); //Процесс удаления всех "нулевых" групп
        return finalGroups; //Возвращаем готовый список
    }

    public static boolean isBadLine(String lineToCheck, List<String> lineOwner){ //Здесь идет проверка того что строка подходит по формату
        if((lineToCheck!=null)&&(lineOwner.size()>0))
        {
            String[] lineArray = lineToCheck.split(";");
            for (int i = 0; i < lineArray.length; i++) {
                long count = lineArray[i].chars().filter(ch -> ch == '\"').count(); //Она не подходит если хотя бы один ее элемент содержит больше двух кавычек
                if (count > 2) {
                    lineOwner.remove(lineToCheck);
                    return true;
                }
            }
            return false;
        }
        return true; //Или если она или ее "хозяин" пустые.
    }

    public static List<String> readTxtToList() throws FileNotFoundException { //Здесь проходит чтение из файла
        Scanner sc = new Scanner(new File(path));
        List<String> myList = new ArrayList<>();
        while(sc.hasNextLine()) {
            String line = sc.nextLine(); //Каждая новая строка в txt файле = новая строка в списке
            myList.add(line);
        }
        return myList;
    }
}